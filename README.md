# H4C Olive

- See #3 for now
- Based on olivero 10.0-dev#1f171b93

## Klären
- Taxonomy page strukturierte Inhalte
  - Bild
  - Text
  - => Wird gebraucht um Taxonomy view sinnvoll zu erstellen
  - => + Themenseiten, wir wollen nicht das layout jeder Seite überschreiben
- Woraus werden social media previews generiert?
  - Betrifft alle nodes, wo previews bearbeitbar sein sollen
  - Aber insbesondere flexi + taxonomy page weil sie ohne felder keine defaults haben
- map_popup depends on specific block template suggestion block--views-block--h4c-map-block-facets to load library

## Card pattern
- Links werden nur in twig.html hinzugefügt, also keine links in field formattern verwenden
  - card component wird auch in blöcken genutzt, die nicht zur entity verlinken, sondern zu einer linkfeld uri
  - URI kommt aus einem ui pattern setting + token im view mode
- Wir packen nicht die komplette Karte in einen link packen 
  - siehe https://css-tricks.com/block-links-the-search-for-a-perfect-solution/

## Colors
Which color format do we use?
- HSL color values are intuitive: Hue (360⁰), Saturation, Lightness: https://www.smashingmagazine.com/2021/07/hsl-colors-css/
  - Lightness is different among colors, so don't get consistent results when using a fixed lightness value in theme css
  - Perceived colors are different, this might also be true for RGB
    - https://lea.verou.me/2021/03/inverted-lightness-variables/#the-problem-with-hsl
  - Olivero uses HSL internally in its new color picker which automatically calculates color variants of a given hex color
- RGBA color values are not intuitive, but lightness is consistent (100 for every color and below in theme css)
  - We can't control saturation

## Requirements
- drupal:block
- drupal:layout_builder
- components:components 3.0.x
- component_blocks:component_blocks
- ui_patterns:ui_patterns_library
- ui_patterns_settings:ui_patterns_settings
- ui_patterns_layout_builder:ui_patterns_layout_builder

## Todo
- ui_patterns: Test nesting atoms into molecules, see https://github.com/nuvoleweb/ui_patterns/issues/245
  - Nesting in twig: https://gitlab.com/geeks4change/site-h4c-dev-local/-/blob/40-uipatterns-39/web/themes/custom/h4c_olive/components/03_organisms/map_popup/pattern-map-popup.html.twig
- lb_columns: For regions to be drag'n'dropable we would need to apply region_attributes['first'] to the actual regions + adding our own class region__content; unlike in drupal core region_attributes.first.addClass('region__title') only outputs classes when editing a layout builder page, but not when viewing it
  - According to upstream region__attributes should work: [Can't drag & drop [#3118765] | Drupal.org](https://www.drupal.org/project/ui_patterns_layout_builder/issues/3118765)
- Test subtheming
- Remove all dependencies and references to olivero, include in h4c_olive

## How this works

H4C Olive is an olivero fork, still we use best pracitces and some code from olivero. We follow CUBE CSS and BEM approaches.

### Files
- `h4c_olive.info.yml`/`h4c_olive.libraries.yml`
  - Reference olivero assets in `../olivero` (olivero 10.0.x required), needs to be internalised before production
  - For subtheming library overrides from info.yml need to be copied
- `h4c_olive.theme`: Copied from olivero
- `/components`: patterns and other componentish styles in atomic structure (css,js,twig)
  - components that are not loaded via ui_pattern are loaded 
    - via h4c_olive.libraries.yml (navigation, site header, etc)
    - via twig attach_library function (menu-local-tasks.html.twig)
  - patterns can nevertheless be loaded globally, e.g. button styles if we need them on the majority of pages
- `css`: overrides of olivero css, added via libraries.yml
- `js`: globally loaded js
- `scripts`: postcss from core https://git.drupalcode.org/project/drupal/-/tree/10.0.x/core/scripts
- `templates`: olivero copies of templates

### Empty regions

We use twig_real_content to avoid displaying empty regions and sections, see components > organisms > lb_columns

## Child theme
### Components
When copying a pattern to a child theme
- the twig file from child theme is picked up automatically
- only the css from the parent theme is picked up
  - => Add asset or only overrides, to child libraries.yml

## How to update h4c_olive parts from olivero

- Check files with upstream changes: `git -C core/themes/olivero/ diff --relative --name-status 1f171b93`
- Check upstream changes in detail, e.g. in twig files: `git -C core/themes/olivero/ diff --relative 1f171b93 HEAD *.twig`
- Implement changes in h4c_olive if applicable
  - `templates/olivero`, possibly `templates/h4c_olive`
  - css: site-header, nav-secondary, tabs

## Z-index

Not so important anymore, because olivero made sure that child elements cannot overlap with (mobile) navigation
- [Olivero: Elements within main content that have a high z-index will appear in front of mobile nav [#3250357] | Drupal.org](https://www.drupal.org/project/drupal/issues/3250357)

- `.leaflet-container`: 1
  - `.leaflet-pane`: 400
    - `.leaflet-tile-pane`: 200
      - `leaflet-layer`: 1
    - `.leaflet-shadow-pane`: 500
    - `.leaflet-overlay-pane`: 400
    - `.leaflet-marker-pane`: 600
      - `.leaflet-marker-icon`: 64
    - `.leaflet-tooltip-pane`: 650
    - `.leaflet-control-container`
      - * : 1000
- `.leaflet-popup-pane`: 700

- [Z-indexes in Drupal 8 | Theming Drupal | Drupal Wiki guide on Drupal.org](https://www.drupal.org/docs/theming-drupal/z-indexes-in-drupal-8)

## UI Patterns

### What does not work
- We can't check a field value by `{% if patternfield is not empty %}`
  - `{% if patternfield is not empty %}` only checks if the field exists
  - instead we need to do `{% if patternfield.0 is not empty %}` or so
- A check like `{% if patternfield.0 is not empty %}` is not properly performed for nested patterns
  - in `{{ pattern('short_infos', {event_date: event_date, location: location}, 'si--event--popup') }}` if in the nested short_infos pattern the location field is checked if empty it is never displayed

With ui_patterns module we can:
- see a style guide in `/patterns`
- provide component variants
- map fields via
  - layout builder (via component blocks module)
  - ds-enabled layouts
  - field groups (not tested)
  - ds field settings (not tested)
- use patterns as views rows template (not tested)
- streamline markup by disabling field templates in ds-enabled layouts (not possible via component blocks)

With ui_patterns_settings module we can
- provide settings
- map settings to drupal fields, e.g. article highlighted field creates a class `is-highlighted`

### On the fly creation of flexible blocks

- Block layout (via component_blocks): 
  - Create block (content) 
  - Overwrite layout (settings) 
    - OK!
- Layout builder (via component_blocks):
  - Create block (content) 
  - Overwrite layout (settings) 
    - ! we can't do this for inline blocks, either create reusable block before, or don't use component_blocks
- Layout builder (via ui_patterns_layout_builder)
  - Create section to which the component is attached
  - Add blocks (inline or component_blocks) to each section
    - ! needs workaround for button, bc it works differently in component_blocks and ui_patterns_lb
    - => See frontpage

**Intermediate solution**
- Create section component via ui_patterns_layout_builder to select number of columns (variant/ui_patterns_settings)
  - => See lb_columns
  - In LB add section
  - In BL configure block with layout override first, add section, add block to block layout
- Create single components
  - heading (with subheading)
  - text
  - image
  - button
  - single node
  - lists

### Based on

- [LPC: Landing page components (#42) · Issues · geeks4change / Sites / site-h4c-multi · GitLab](https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/42)
- [LPC: Figure out component architecture and tools (#43) · Issues · geeks4change / Sites / site-h4c-multi · GitLab](https://gitlab.com/geeks4change/sites/site-h4c-multi/-/issues/43)
- [LPC: Test UI Patterns (#40) · Issues · geeks4change / site-h4c-dev-local · GitLab](https://gitlab.com/geeks4change/site-h4c-dev-local/-/issues/40)

### Known issues

- No maintainer activity in pattern_ui issue queue sine mid 2020: https://github.com/nuvoleweb/ui_patterns/issues
- We cannot create a full static website from components (clickdummy), a more elaborated setup is needed (emulsify, storybook)
- For storybook integration see https://wingsuit-designsystem.github.io/

# Check back later

## Theming basics
- css/js: declare as libraries in libraries.yml
- global libraries to be loaded on every page, add to info.yml
- conditional libraries to be loaded by twig or otherwise
  - add `preprocess: false` so the cache is not rebuilt for them
  - check if postcss compresses files

## Resources

## Icons

- https://icon-sets.iconify.design/

### css

- [CUBE CSS | CUBE CSS](https://cube.fyi/#what-does-cube-css-stand-for)
- [Inline-block to CSS Grid](https://achrafkassioui.com/blog/inline-block-to-css-grid/)
- [What No One Told You About Z-Index — Philip Walton](https://philipwalton.com/articles/what-no-one-told-you-about-z-index/)
- [Understanding & Debugging Stacking Contexts (and the Z-Index) | Lullabot](https://www.lullabot.com/articles/understanding-debugging-stacking-contexts)

### drupal

- [Performance improvements with Drupal 8 Libraries | PreviousNext](https://www.previousnext.com.au/blog/performance-improvements-drupal-8-libraries)
- [CSS file organization (for Drupal 9) | CSS | Drupal Wiki guide on Drupal.org](https://www.drupal.org/docs/develop/standards/css/css-file-organization-for-drupal-9#modules)


Credits: Tree in logo taken from: https://pixabay.com/vectors/tree-tree-of-life-frame-spiritual-5334773/

## Tests

Expandable description with alpinejs

https://github.com/alpinejs/alpine/issues/406#issuecomment-617771839

```

.expand-content {
  transition-duration: .2s;
  transition-timing-function: cubic-bezier(.4,0,1,1);
  transition-property: all;
  position: relative;
  overflow: hidden;
}

.expand-button {
  z-index: 20;
  bottom: 0;
  right: 0;
  position: absolute;
  overflow: visible;
  margin-left: auto;
  margin-right: auto;
}

.max-h-40 {
  max-height: 3rem;

}

```

    <div class="expand-wrapper">
      <div x-data="{ expanded: false }">
     
        <div class="expand-content" x-bind:class="{'max-h-40': !expanded}" x-ref="container" x-bind:style="expanded ? 'max-height: ' + $refs.container.offsetHeight + 'px' : ''">

          {{ description }}

          <button class="expand-button" type="button" @click="expanded = !expanded">
            <em ></em> <span x-text="expanded ? 'Weniger anzeigen' : 'Weiterlesen'"></span>
          </button>
        </div>
      </div>
    </div>
