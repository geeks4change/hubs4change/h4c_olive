(function ($) {
  'use strict';
  $(document).on('leaflet.map', function (e, settings, lMap) {

    // Move popupPane out of map container according to https://gis.stackexchange.com/a/186169
    // Avoid overflow hidden issue in iOS/Safari which makes it unusable + works in IE11 (before the popup did not load)

    // Get the popupPane
    var htmlObject = lMap.getPane('popupPane');
    // Move pane into map section
    var a = document.querySelector('.js-h4c-popup');

    $(htmlObject).addClass('h4c-popup'); /* Because we move the element out of the parent container, we need to apply the class for styling again */
    // Finally append that node to the new parent, recursively searching out and re-parenting nodes.

    function setParent(el, newParent)
    {
      newParent.appendChild(el);
    }
    setParent(htmlObject, a);
    
  });
  // Fixed position popups seem not to play well with autopan.
  $(document).on('leaflet.feature', function (e, lFeature, feature, drupalLeaflet) {
    lFeature._popup.options.autoPan = false;
  });

})(jQuery);
